Source: lz4tools
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Michal Arbet <michal.arbet@ultimum.io>,
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all-dev,
 python3-pbr (>= 2.0.0),
 python3-pytest,
 python3-setuptools,
Standards-Version: 4.1.3
Homepage: https://github.com/darkdragn/lz4tools
Vcs-Git: https://salsa.debian.org/openstack-team/python/python-lz4tools.git
Vcs-Browser: https://salsa.debian.org/openstack-team/python/python-lz4tools

Package: python3-lz4tools
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Tools for working with LZ4 compression algorithm.
 LZ4 is lossless compression algorithm, providing compression speed > 500 MB/s
 per core, scalable with multi-cores CPU. It features an extremely fast
 decoder, with speed in multiple GB/s per core, typically reaching RAM speed
 limits on multi-core systems.
 .
 Speed can be tuned dynamically, selecting an "acceleration" factor which
 trades compression ratio for faster speed. On the other end, a high
 compression derivative, LZ4_HC, is also provided, trading CPU time for
 improved compression ratio. All versions feature the same decompression speed.
 .
 LZ4 is also compatible with dictionary compression, and can ingest any input
 file as dictionary, including those created by Zstandard Dictionary Builder.
